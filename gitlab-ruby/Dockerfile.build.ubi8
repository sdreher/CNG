ARG BUILD_IMAGE=

FROM ${BUILD_IMAGE}

ARG RUBY_MAJOR_VERSION=2.6
ARG RUBY_MINOR_VERSION=2.6.6
ARG RUBYGEMS_VERSION=2.7.10
ARG BUNDLER_VERSION=1.17.3
ARG RBREADLINE_VERSION=0.5.5

ENV LANG=C.UTF-8

COPY shared/build-scripts/ /build-scripts

RUN mkdir /assets \
    && curl --retry 6 -s https://cache.ruby-lang.org/pub/ruby/${RUBY_MAJOR_VERSION}/ruby-${RUBY_MINOR_VERSION}.tar.gz | tar -xz \
    && cd ruby-${RUBY_MINOR_VERSION} \
    && ./configure --prefix=/usr --disable-dtrace --disable-install-doc --disable-install-rdoc --enable-shared --with-out-ext=dbm,readline --without-gmp --without-gdbm --without-tk \
    && make -j "$(nproc)" \
    && make -j "$(nproc)" install \
    && gem update --no-document --system ${RUBYGEMS_VERSION} \
    && gem install bundler --version ${BUNDLER_VERSION} --force --no-document \
    && cd .. \
    && curl --retry 6 -sL https://github.com/connoratherton/rb-readline/archive/v${RBREADLINE_VERSION}.tar.gz | tar -xz \
    && ruby rb-readline-${RBREADLINE_VERSION}/setup.rb \
    && /build-scripts/cleanup-gems /usr/lib/ruby/gems \
    && cp -R --parents \
      /usr/bin/{ruby,rdoc,irb,erb,rake,gem,bundler,bundle} \
      /usr/lib/{ruby/,libruby.*} \
      /usr/include/ruby-*/ \
      /assets
