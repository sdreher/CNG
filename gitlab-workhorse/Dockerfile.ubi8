ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG WORKHORSE_VERSION=v5.2.0
ARG GITLAB_USER=git
ARG GITLAB_DATA=/var/opt/gitlab
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/gitlab-workhorse" \
      name="GitLab Workhorse" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${WORKHORSE_VERSION} \
      release=${WORKHORSE_VERSION} \
      summary="Gitlab Workhorse is a smart reverse proxy for GitLab." \
      description="Gitlab Workhorse is a smart reverse proxy for GitLab. It handles large HTTP requests."

ENV LANG=C.UTF-8

ADD gitlab-workhorse-ee.tar.gz /

COPY scripts/ /scripts/

RUN dnf clean all \
    && rm -r /var/cache/dnf \
    && dnf ${DNF_OPTS} install -by --nodocs perl \
    && adduser -m ${GITLAB_USER} \
    && mkdir -p /var/log/gitlab /srv/gitlab/config ${GITLAB_DATA} \
    && chown -R ${GITLAB_USER}:${GITLAB_USER} \
        /srv/gitlab \
        /var/log/gitlab \
        ${GITLAB_DATA}

USER ${GITLAB_USER}:${GITLAB_USER}

CMD /scripts/start-workhorse

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
